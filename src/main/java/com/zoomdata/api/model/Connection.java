package com.zoomdata.api.model;

public class Connection {

    private final String baseUri;
    private final String user;
    private final String password;

    public Connection(String baseUri, String user, String password) {
        this.baseUri = baseUri;
        this.user = user;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Connection{" +
                "baseUri='" + baseUri + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getBaseUri() {
        return baseUri;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
