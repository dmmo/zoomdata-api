(ns zoomdata.http
  (:require [cheshire.core :as json]
            [clj-http.client :as http])
  (:import (com.zoomdata.api.model Connection)))

(def ^:dynamic *auth* [])

(def ^:dynamic *http-opts*
  {:accept           :json
   :as               :json-string-keys
   :basic-auth       *auth*
   :content-type     :json
   :throw-exceptions true})

(defmacro with-connection
  [c & body]
  `(binding [*auth* [(.user ~c) (.password ~c)]]
     ~@body))

(defn delete
  [^Connection conn uri]
  (with-connection conn
    (let [r (http/delete (str (.baseUri conn) "/" uri) *http-opts*)]
     (json/decode (:body r) true))))

(defn get
  [^Connection conn uri]
  (with-connection conn
   (let [r (http/get (str (.baseUri conn) "/" uri) *http-opts*)]
     (json/decode (:body r) true))))

(defn post
  [^Connection conn uri & [opts]]
  (with-connection conn
    (let [r (http/post (str (.baseUri conn) "/" uri) (merge *http-opts* opts))]
      (json/decode (:body r) true))))