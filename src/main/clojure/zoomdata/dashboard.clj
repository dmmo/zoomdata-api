(ns zoomdata.dashboard
  (:require [zoomdata.http :as http]
            [zoomdata.visualizations :as v]
            [clojure.string :as str]
            [cheshire.core :refer [generate-string]])
  (:import (java.util Date UUID)))

(defn- generate-id []
  (str (UUID/randomUUID)))

(defn make-bookmark
  [name source vis vis-cfg]
  (let [time-field (get source "timeFieldName")
        created-date (str (Date.))
        vis-type (get vis "type")]
    {"name"            name
     "description"     ""
     "layout"          "unset"
     "rememberTime"    false
     "shareState"      "VIEW_ONLY"
     "showDescription" false
     "unifiedBarCfgs"  []
     "visualizations"
                       [{"visId"            (get vis "id")
                         "dashboardLink"    {"inheritTimeCfg" true}
                         "controlsCfg"      {"timeControlCfg" {"timeField" time-field} "playerControlCfg" {}}
                         "widgetId"         (generate-id)
                         "type"             vis-type
                         "enabled"          false
                         "lastModifiedDate" created-date
                         "viewsCount"       0
                         "showDescription"  false
                         "thumbnailDate"    0
                         "createdDate"      created-date
                         "name"             (get vis "name")
                         "source"           vis-cfg
                         "variables"        (get vis "variables")
                         "owner"            "BOOKMARK"
                         "layout"           "unset"}]}))

(defn ^:private make-metric-cfg
  [metric metric-field]
  (if (= "count" metric)
    "count"
    (str (str/lower-case metric-field) ":" metric)))

(defmulti ^:private set-vis-variables (fn [type & _] type))

(defmethod set-vis-variables "PIE"
  [_ vis-cfg group-by-field metric metric-field]
  (let [metric-cfg (make-metric-cfg metric metric-field)
        group-name (get group-by-field "name")
        group-label (get group-by-field "label")
        group-cfg {"sort"                {"dir"  "asc"
                                          "name" group-name}
                   "limit"               1000
                   "name"                group-name
                   "label"               group-label
                   "type"                "ATTRIBUTE"
                   "groupColorSet"       "ZoomPalette"
                   "autoShowColorLegend" true
                   "colorNumb"           5
                   "autoColor"           true}]

    (-> vis-cfg
        (assoc-in ["variables" "Group By"] (generate-string group-cfg))
        (assoc-in ["variables" "Size"] metric-cfg))))

(defmethod set-vis-variables "UBER_BARS"
  [_ vis-cfg group-by-field metric metric-field]
  (let [group-cfg [{"name"                (get group-by-field "name")
                    "limit"               50
                    "sort"                {"dir" "desc" "name" "count"}
                    "label"               (get group-by-field "label")
                    "type"                "ATTRIBUTE"
                    "groupColorSet"       "ZoomSequential"
                    "autoShowColorLegend" false
                    "colorNumb"           2
                    "autoColor"           true
                    "groupColors"         {}}]
        metric (make-metric-cfg metric metric-field)]
    (-> vis-cfg
        (assoc-in ["variables" "Multi Group By"] (generate-string group-cfg))
        (assoc-in ["variables" "Metric"] metric))))


(defn ^:private make-vis-cfg
  [source vis group-by-field metric metric-field]
  (let [vis-id (get vis "id")
        vis-type (get vis "type")
        cfg (first (filter #(= vis-id (get % "visId")) (get source "visualizations")))
        group-by-field (first (filter #(= group-by-field (get % "name")) (get source "objectFields")))
        vis-cfg (get cfg "source")]
    (set-vis-variables vis-type vis-cfg group-by-field metric metric-field)))

(defn create
  [conn bookmark-name source {:keys [vis-type metric metric-field group-by]}]
  (let [vis (v/get-by-type conn vis-type)
        vis-cfg (make-vis-cfg source vis group-by metric metric-field)
        bookmark (make-bookmark bookmark-name source vis vis-cfg)]
    (http/post conn "/service/bookmarks/" {:body bookmark})))

(defn delete
  [conn id]
  (let [uri (str "/zoomdata/api/dashboards/" id)]
    (http/delete conn uri)))
