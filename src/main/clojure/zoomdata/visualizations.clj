(ns zoomdata.visualizations
  (:require [zoomdata.http :as http])
  (:import (com.zoomdata.api.model Connection)))

(defn get-by-type
  [^Connection conn type]
  (let [resp (http/get "/service/visualizations/" conn)
        vis (first (filter #(= type (% "type")) resp))]
    (if vis
      vis
      (throw (IllegalArgumentException. (str "Couldn't find a vis with type " type))))))