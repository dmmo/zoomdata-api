(ns zoomdata.api
  (:require [zoomdata.dashboard :as bookmark]
            [zoomdata.source :as source])
  (:gen-class
    :name com.zoomdata.api.ZoomdataApi
    :methods [[createSource [String String String String] String]
              [createDashboard [String String String String String String] void]
              [deleteDashboard [String] void]]
    :constructors {[com.zoomdata.api.model.Connection] []}
    :state connection
    :init init)
  (:import (com.zoomdata.api.model Connection)))

(defn -init [^Connection connection]
  [[] connection])
;
(defn
  ^{:doc "Creates source"}
  -createSource
  [this sourceName connId tableName schemaName]
  (let [conn (.connection this)
        source (source/create conn sourceName connId tableName schemaName)]
    (get source "id")))

(defn -createDashboard
  [this bookmark-name sourceId visType metric metricField groupBy]
  (let [conn (.connection this)
        source (source/get-by-id conn sourceId)
        config {:vis-type visType :metric metric :metric-field metricField :group-by groupBy}]
    (bookmark/create conn bookmark-name source config)))

(defn -deleteDashboard
  [this id]
  (bookmark/delete id (.connection this)))
