(ns zoomdata.source
  (:require [zoomdata.http :as http]
            [clojure.string :as str]
            [cheshire.core :refer [generate-string]]))

(defn ^:private source-id
  "Extracts source-id from response after source was created"
  [source]
  (-> (get source "links")
      first
      (get "href")
      (str/split #"/")
      last))

(defn create
  [conn source-name conn-id table schema]
  (println (format "Creating source: '%s'" source-name))
  (let [body (generate-string {"name"             source-name
                               "sourceParameters" {"resource" table
                                                   "schema"   schema}})
        url (format "/api/connections/%s/sources" conn-id)
        resp (http/post conn url {:body body})]
    (if (= (:status resp) 201)
      (source-id (:body resp))
      (throw (Exception. (str "Failed to create source: " (:body resp)))))))

(defn get-by-id
  [conn id]
  (http/get conn (str "/service/sources/" id)))

(defn get-by-name
  [conn source-name]
  (let [sources (http/get conn "/service/sources?fields=id,name")
        source-id (some #(if (= source-name (get % "name")) (get % "id")) sources)]
    (when source-id
      (get-by-id conn source-id))))