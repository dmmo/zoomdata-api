# What is this?

This is a Java client for Zoomdata REST API, which simplifies creation for Zoomdata dashboards.

# Installation
Install Leiningen with `brew install leiningen`. Clone the project, build and install into local Maven repository:
```
git clone git@bitbucket.org:dmmo/zoomdata-api.git
cd zoomdata-api
lein install
```

Add it as dependency to you Java program.
```
<dependency>
  <group>com.zoomdata</group>
  <artifactId>api-client</artifactId>
  <version>0.1.0-SNAPSHOT</version>
</dependency>
```

# Usage
```
package com.zoomdata.mesos;

import com.zoomdata.api.ApiClient;
import com.zoomdata.api.ZoomdataApi;

public class ClientTest {

    public static void main(String[] args) {
        final ApiClient client = new ApiClient("http://localhost:8080", "admin", "Z00mda1a");
        final ZoomdataApi api = new ZoomdataApi(client);

        final String connectionId = "56fa672dbee84a87753ea478";
        final String sourceName = "Zoomdata Twitter Mentions";
        final String tableName = "tweets_zoomdata";
        final String schemaName = "kafka";
        final String sourceId = api.createSource(sourceName, connectionId, tableName, schemaName);

        final String bookmarkName = "Zoomdata Tweets";
        final String visType = "UBER_BARS";
        final String metric = "sum";
        final String metricField = "followers_count";
        final String groupByField = "user_location";

        api.createBookmark(bookmarkName, sourceId, visType, metric, metricField, groupByField);
    }
}
```

# Known issues
- Only following visualizations are supported: UBER_BARS, PIE
- Only EDC source can be created